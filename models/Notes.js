const mongoose = require('mongoose');

const noteSchema =  new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true 
  },
  completed: {
    type: Boolean,
    default: false
  },
  username: {
    type: String,
    required: true,
    unique: false
  },
  text: {
    type: String
  },
  createdDate: {
    type: String,
    required: true
  },
  updatedDate: {
    type: String,
    required: true
  }
});

const Note = mongoose.model('note', noteSchema);

module.exports = {
  Note
};
