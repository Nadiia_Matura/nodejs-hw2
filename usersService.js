const { User } = require('./models/Users.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const createProfile = async (req, res, next) => {
  const { username, password } = req.body;
  const registeredUser = await User.findOne({ username: username });
  const currentDate = new Date().toString();

  const user = new User({
    username: username,
    password: await bcrypt.hash(password, 10),
    createdDate: currentDate,
    updatedDate: currentDate
  });

  if (!username) {
    return res.status(400).json({ 'message': 'Wrong username' });
  } else if (!password) {
    return res.status(400).json({ 'message': 'Wrong password' });
  } else if (registeredUser) {
    return res.status(400).json({ 'message': 'This user is already registered' });
  } else {
    user.save()
      .then(saved => res.status(200).json({ 'message': `User ${username} is created` }))
      .catch(err => {
        next(err);
      });
  };
}

const login = async (req, res, next) => {
  const user = await User.findOne({ username: req.body.username });
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = { username: user.username, password: user.password, userId: user._id };
    const jwtToken = jwt.sign(payload, 'secret-jwt-key');
    return res.status(200).json({ jwt_token: jwtToken, 'message': 'User is authorized' });
  } else {
    return res.status(400).json({ 'message': 'Not authorized' });
  }
}

const getProfileInfo = async (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) {
    return res.status(401).json({ 'message': 'Please, provide authorization header' });
  }

  const [, token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ 'message': 'Please, include token to request' });
  }

  const tokenPayload = jwt.verify(token, 'secret-jwt-key');
  const username = tokenPayload.username;
  const userMe = await User.findOne({ username: username });

  if (userMe.username) {
    return res.status(200).json({ 'user': userMe });
  } else {
    return res.status(401).json({ 'message': 'User is not found' });
  }
}

const changeProfilePassword = async (req, res, next) => {
  const { authorization } = req.headers;
  const { oldPassword, newPassword } = req.body;
  const hashOldPassword = await bcrypt.hash(oldPassword, 10);
  const hashNewPassword = await bcrypt.hash(newPassword, 10);

  if (!authorization) {
    return res.status(401).json({ 'message': 'Please, provide authorization header' });
  }
  const [, token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ 'message': 'Please, include token to request' });
  }
  
  const tokenPayload = jwt.verify(token, 'secret-jwt-key');
  const user = {
    userId: tokenPayload.userId,
    username: tokenPayload.username,
    password: tokenPayload.password
  };

  if (User.findOne({ username: user.username })) {
    User.updateOne({ password: hashOldPassword }, { $set: { password: hashNewPassword }}, (err) => {
      if (err) {
        return res.status(400).json({ 'message': 'Password is not changed' });
      } else {
        return res.status(200).json({ 'message': 'Password is changed' });
      }
    });
  }
}

const deleteProfile = async (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) {
    return res.status(401).json({ 'message': 'Please, provide authorization header' });
  }

  const [, token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ 'message': 'Please, include token to request' });
  }

  const tokenPayload = jwt.verify(token, 'secret-jwt-key');
  const username = tokenPayload.username;

  User.findOne({ user: { username: username } })
    .then(doc => {
      User.deleteOne({ user: { _id: doc._id } });
      return res.status(200).json({ 'message': 'Profile is deleted' });
    })
}

module.exports = {
  createProfile,
  login,
  getProfileInfo,
  changeProfilePassword,
  deleteProfile
};
