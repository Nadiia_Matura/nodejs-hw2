const express = require('express');
const router = express.Router();
const {
  addUserNotes, getUserNotes, getUserNoteById, deleteUserNoteById, updateUserNoteById, toggleCompletedForUserNoteById
} = require('./notesService.js');

router.post('/', addUserNotes);
router.get('/', getUserNotes);
router.get('/:id', getUserNoteById);
router.delete('/:id', deleteUserNoteById);
router.put('/:id', updateUserNoteById);
router.patch('/:id', toggleCompletedForUserNoteById);

module.exports = {
  notesRouter: router
};
