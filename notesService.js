const { Note } = require('./models/Notes.js');
const jwt = require('jsonwebtoken');

const authJWT = (auth) => {
  if (!auth) {
    return res.status(401).json({ 'message': 'Please, provide authorization header' });
  }
  const [, token] = auth.split(' ');
  if (!token) {
    return res.status(401).json({ 'message': 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, 'secret-jwt-key');
    return tokenPayload;
  } catch (err) {
    return res.status(401).json({ message: err.message });
  }
}

const addUserNotes = (req, res, next) => {
  const text = req.body.text;
  const { authorization } = req.headers;
  const user = authJWT(authorization);
  const currentDate = new Date().toString();

  const note = new Note({
    userId: user.userId,
    text: text,
    username: user.username,
    createdDate: currentDate,
    updatedDate: currentDate
  });

  note.save()
    .then(saved => res.status(200).json({ 'message': 'Success' }))
    .catch(err => {
      next(err);
    });
}

const getUserNotes = (req, res, next) => {
  const { authorization } = req.headers;
  const user = authJWT(authorization);
  const userNotes = [];

  Note.find({ username: user.username })
    .then((result) => {
      userNotes.push(result);
      return res.status(200).json({ 'notes': userNotes });
    })
    .catch(err => {
      next(err);
    })
}

const getUserNoteById = (req, res, next) => Note.findById(req.params.id)
  .then((note) => {
    res.status(200).json({ 'note': note });
  });

const deleteUserNoteById = (req, res, next) => {
  Note.findByIdAndDelete(req.params.id)
    .then((note) => {
      res.status(200).json({ 'message': 'Success' });
    });
}

const updateUserNoteById = (req, res, next) => {
  const text = req.body.text;
  return Note.findByIdAndUpdate({ _id: req.params.id }, { $set: { text: text } })
    .then((result) => {
      res.status(200).json({ 'message': 'Success' });
    });
}

const toggleCompletedForUserNoteById = (req, res, next) => {
  return Note.findByIdAndUpdate({ _id: req.params.id }, { $set: { completed: true } })
    .then((result) => {
      res.json({ 'message': 'Success' });
    });
}

module.exports = {
  addUserNotes,
  getUserNotes,
  getUserNoteById,
  deleteUserNoteById,
  updateUserNoteById,
  toggleCompletedForUserNoteById
};
