const express = require('express');
const router = express.Router();
const { createProfile, login, getProfileInfo, changeProfilePassword, deleteProfile } = require('./usersService.js');

router.post('/auth/register', createProfile);
router.post('/auth/login', login);
router.get('/users/me', getProfileInfo);
router.patch('/users/me', changeProfilePassword);
router.delete('/users/me', deleteProfile);

module.exports = {
  usersRouter: router
};
